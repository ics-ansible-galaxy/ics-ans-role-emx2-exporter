# ics-ans-role-emx2-exporter

Ansible role to install emx2-exporter.

## Role Variables

```yaml
---
emx2_exporter_repository: https://gitlab.esss.lu.se/ics-infrastructure/emx2-exporter.git
emx2_exporter_main_path: /opt/emx2_exporter
emx2_exporter_config_dir: /etc/emx2_exporter
emx2_exporter_version: master

emx2_exporter_group: emx2exporters
emx2_exporter_user: emx2exporter
emx2_exporter_bind: 127.0.0.1:8000

emx2_exporter_env_file: "{{ emx2_exporter_config_dir }}/emx2_exporter_env.yml"


```

## Example Playbook

```yaml
- hosts: emx2exporters
  roles:
    - role: ics-ans-role-emx2-exporter
```

## License

BSD 2-clause
